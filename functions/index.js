const functions = require("firebase-functions");
const auth = require("./util/auth");
const app = require("express")();
const cors = require("cors");

app.use(cors());
app.options("*", cors());

const {
  getAllProjects,
  postOneProject,
  deleteProject,
  editProject,
  likeProject,
  unlikeProject,
  dislikeProject,
  undislikeProject,
} = require("./APIs/projects");

const {
  loginUser,
  signUpUser,
  uploadProfilePhoto,
  getUserDetail,
  updateUserDetails,
} = require("./APIs/users.js");

app.post("/login", loginUser);
app.post("/signup", signUpUser);
app.post("/user/image", auth, uploadProfilePhoto);
app.get("/user", auth, getUserDetail);
app.post("/user", auth, updateUserDetails);
app.get("/projects", auth, getAllProjects);
app.post("/projects", auth, postOneProject);
app.delete("/projects/:projectId", auth, deleteProject);
app.put("/projects/:projectId", auth, editProject);
app.get("/like/:projectId", auth, likeProject);
app.get("/unlike/:projectId", auth, unlikeProject);
app.get("/dislike/:projectId", auth, dislikeProject);
app.get("/undislike/:projectId", auth, undislikeProject);

exports.api = functions.region("europe-west1").https.onRequest(app);
