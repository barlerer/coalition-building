const { admin, db } = require("../util/admin");

exports.getAllProjects = (request, response) => {
  db.collection("projects")
    .orderBy("plandate", "desc")
    .get()
    .then((data) => {
      let todos = [];
      data.forEach((doc) => {
        todos.push({
          projectId: doc.id,
          title: doc.data().title,
          description: doc.data().description,
          plandate: doc.data().plandate,
          likes: doc.data().likes,
          dislikes: doc.data().dislikes,
          imageurl: doc.data().image,
          createdBy: doc.data().createdBy,
          location: doc.data().location,
        });
      });
      return response.json(todos);
    })
    .catch((err) => {
      console.error(err);
      return response.status(500).json({ error: err.code });
    });
};

exports.postOneProject = (request, response) => {
  if (request.body.description.trim() === "") {
    return response.status(400).json({ description: "Must not be empty" });
  }

  if (request.body.title.trim() === "") {
    return response.status(400).json({ title: "Must not be empty" });
  }

  const newProjectItem = {
    createdBy: request.user.email,
    title: request.body.title,
    description: request.body.description,
    plandate: admin.firestore.Timestamp.fromDate(
      new Date(Date.parse(request.body.plandate))
    ),
    likes: [],
    image: request.body.image,
  };
  db.collection("projects")
    .add(newProjectItem)
    .then((doc) => {
      const responseProjectItem = newProjectItem;
      responseProjectItem.id = doc.id;
      return response.json(responseProjectItem);
    })
    .catch((err) => {
      response.status(500).json({ error: "Something went wrong" });
      console.error(err);
    });
};

exports.deleteProject = (request, response) => {
  const document = db.doc(`/projects/${request.params.projectId}`);
  document
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return response.status(404).json({ error: "Project not found" });
      }
      return document.delete();
    })
    .then(() => {
      return response.json({ message: "Delete successfull" });
    })
    .catch((err) => {
      console.error(err);
      return response.status(500).json({ error: err.code });
    });
};

exports.editProject = (request, response) => {
  if (request.body.projectId) {
    response.status(403).json({ message: "Not allowed to edit" });
  }
  let document = db.collection("projects").doc(`${request.params.projectId}`);
  document
    .update(request.body)
    .then(() => {
      return response.json({ message: "Updated successfully" });
    })
    .catch((err) => {
      console.error(err);
      return response.status(500).json({
        error: err.code,
      });
    });
};

exports.likeProject = (request, response) => {
  const newUserLike = request.user.email;
  let document = db.collection("projects").doc(`${request.params.projectId}`);
  document
    .update({ likes: admin.firestore.FieldValue.arrayUnion(newUserLike) })
    .then(() => {
      return response.json({ message: "Like added successfully" });
    })
    .catch((err) => {
      console.error(err);
      return response.status(500).json({
        error: err.code,
      });
    });
};

exports.unlikeProject = (request, response) => {
  const newUserLike = request.user.email;
  let document = db.collection("projects").doc(`${request.params.projectId}`);
  document
    .update({ likes: admin.firestore.FieldValue.arrayRemove(newUserLike) })
    .then(() => {
      return response.json({ message: "Like removed successfully" });
    })
    .catch((err) => {
      console.error(err);
      return response.status(500).json({
        error: err.code,
      });
    });
};

exports.dislikeProject = (request, response) => {
  const newUserLike = request.user.email;
  let document = db.collection("projects").doc(`${request.params.projectId}`);
  document
    .update({ dislikes: admin.firestore.FieldValue.arrayUnion(newUserLike) })
    .then(() => {
      return response.json({ message: "dislike added successfully" });
    })
    .catch((err) => {
      console.error(err);
      return response.status(500).json({
        error: err.code,
      });
    });
};

exports.undislikeProject = (request, response) => {
  const newUserLike = request.user.email;
  let document = db.collection("projects").doc(`${request.params.projectId}`);
  document
    .update({ dislikes: admin.firestore.FieldValue.arrayRemove(newUserLike) })
    .then(() => {
      return response.json({ message: "dislike removed successfully" });
    })
    .catch((err) => {
      console.error(err);
      return response.status(500).json({
        error: err.code,
      });
    });
};
