import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import React from "react";
import axios from "axios";
class MapView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [],
    };
  }

  componentDidMount = () => {
    const authToken = localStorage.getItem("AuthToken");
    axios.defaults.headers.common = { Authorization: `${authToken}` };
    axios
      .get("/projects")
      .then((response) => this.setState({ projects: response.data }))
      .catch((err) => console.log(err));
  };
  render() {
    return (
      <MapContainer
        center={[51.903613, 4.455968]}
        zoom={13}
        scrollWheelZoom={true}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {this.state.projects.map((project) => (
          <Marker
            position={[project.location._latitude, project.location._longitude]}
          >
            <Popup maxWidth="auto">
              <h1>{project.title}</h1>
              <img
                src={project.imageurl}
                width="300"
                height="auto"
                alt=""
              ></img>
              <p>{project.description}</p>
            </Popup>
          </Marker>
        ))}
      </MapContainer>
    );
  }
}

export default MapView;
