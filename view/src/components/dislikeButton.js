import React from "react";
import ThumbDownOutlinedIcon from "@material-ui/icons/ThumbDownOutlined";
import ThumbDownIcon from "@material-ui/icons/ThumbDown";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";

const useStyles = (theme) => ({
  thumbsDown: {
    background: "linear-gradient(45deg, #56CCF2 30%, #2F80ED 90%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "white",
    height: 48,
    padding: "0 30px",
  },
});

class DislikeButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      disliked: this.props.disliked,
    };
  }
  async clicked() {
    if (this.state.disliked) {
      await axios.get(`/undislike/${this.props.projectId}`);
    } else {
      await axios.get(`/dislike/${this.props.projectId}`);
    }
    this.setState({ disliked: !this.state.disliked });
  }

  render() {
    const { classes } = this.props;

    return (
      <Button
        className={classes.thumbsDown}
        size="small"
        onClick={() => this.clicked()}
      >
        {this.state.disliked ? <ThumbDownIcon /> : <ThumbDownOutlinedIcon />}
      </Button>
    );
  }
}

export default withStyles(useStyles)(DislikeButton);
