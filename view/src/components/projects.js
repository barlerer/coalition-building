import React from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";

import withStyles from "@material-ui/core/styles/withStyles";
import axios from "axios";

import ProjectDetailsDialog from "../components/projectsDetailsDialog";
import LikeButton from "../components/likeButton";
import DislikeButton from "./dislikeButton";

const useStyles = (theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  projectsGrid: {
    paddingTop: theme.spacing(10),
    paddingBottom: theme.spacing(8),
  },
  project: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  projectMedia: {
    paddingTop: "56.25%", // 16:9
  },
  projectContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  learnMoreButton: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "white",
    height: 48,
    padding: "0 30px",
  },
  thumbsDown: {
    background: "linear-gradient(45deg, #56CCF2 30%, #2F80ED 90%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "white",
    height: 48,
    padding: "0 30px",
  },
  thumbsUp: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "white",
    height: 48,
    padding: "0 30px",
  },
});

class Projects extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [],
    };
  }

  componentDidMount() {
    const authToken = localStorage.getItem("AuthToken");
    axios.defaults.headers.common = { Authorization: `${authToken}` };
    axios
      .get("/projects")
      .then((response) => {
        return this.setState({ projects: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  liked(project) {
    return project.likes.includes(this.props.user_email);
  }

  disliked(project) {
    if (!project.dislikes) return false;
    return project.dislikes.includes(this.props.user_email);
  }

  render() {
    const { classes } = this.props;
    function truncate(string) {
      return string.length > 180 ? string.substr(0, 200) + "..." : string;
    }
    return (
      <React.Fragment>
        <CssBaseline />
        <main>
          {/* Hero unit */}

          <Container className={classes.projectsGrid} maxWidth="lg">
            {/* End hero unit */}
            <Grid container spacing={4}>
              {this.state.projects.map((project) => (
                <Grid item key={project.projectId} xs={12} sm={6} md={4}>
                  <Card className={classes.projects}>
                    <CardMedia
                      className={classes.projectMedia}
                      image={project.imageurl}
                      title={project.title}
                    />
                    <CardContent className={classes.projectContent}>
                      <Typography gutterBottom variant="h5" component="h2">
                        {project.title}
                      </Typography>
                      <Typography>{truncate(project.description)}</Typography>
                    </CardContent>
                    <CardActions>
                      <LikeButton
                        projectId={project.projectId}
                        liked={this.liked(project)}
                      />

                      <DislikeButton
                        projectId={project.projectId}
                        disliked={this.disliked(project)}
                      />
                      <ProjectDetailsDialog
                        title={project.title}
                        description={project.description}
                        likes={project.likes}
                        dislikes={project.dislikes}
                      />
                    </CardActions>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </Container>
        </main>
      </React.Fragment>
    );
  }
}

export default withStyles(useStyles)(Projects);
