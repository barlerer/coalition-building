import React from "react";
import ThumbUpOutlinedIcon from "@material-ui/icons/ThumbUpOutlined";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";

const useStyles = (theme) => ({
  thumbsUp: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "white",
    height: 48,
    padding: "0 30px",
  },
});

class LikeButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      liked: props.liked,
    };
  }
  async clicked() {
    if (this.state.liked) {
      await axios.get(`/unlike/${this.props.projectId}`);
    } else {
      await axios.get(`/like/${this.props.projectId}`);
    }
    this.setState({ liked: !this.state.liked });
  }

  render() {
    const { classes } = this.props;

    return (
      <Button
        className={classes.thumbsUp}
        size="small"
        onClick={() => this.clicked()}
      >
        {this.state.liked ? <ThumbUpIcon /> : <ThumbUpOutlinedIcon />}
      </Button>
    );
  }
}

export default withStyles(useStyles)(LikeButton);
