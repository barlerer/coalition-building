import React from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import login from "./pages/login";
import signup from "./pages/signup";
import home from "./pages/home";
import map from "./pages/map";
import CssBaseline from "@material-ui/core/CssBaseline";

import "./App.css";

import axios from "axios";
axios.defaults.baseURL =
  "https://europe-west1-fir-coalition-build.cloudfunctions.net/api";
function App() {
  return (
    <div>
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/icon?family=Material+Icons"
      />
      <CssBaseline />
      <Router>
        <div>
          <Switch>
            <Route exact path="/login" component={login} />
            <Route exact path="/signup" component={signup} />
            <Route exact path="/" component={home} />
            <Route exact path="/map" component={map} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}
export default App;
