import React, { Component } from "react";
import axios from "axios";

import Projects from "../components/projects";
import withStyles from "@material-ui/core/styles/withStyles";

import { authMiddleWare } from "../util/auth";
import Navbar from "../components/navbar";
import CircularProgress from "@material-ui/core/CircularProgress";

const styles = (theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },

  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  avatar: {
    height: 110,
    width: 100,
    flexShrink: 0,
    flexGrow: 0,
    marginTop: 20,
  },
  uiProgess: {
    position: "fixed",
    zIndex: "1000",
    height: "31px",
    width: "31px",
    left: "50%",
    top: "35%",
  },
  toolbar: theme.mixins.toolbar,
});

class home extends Component {
  state = {
    render: false,
  };

  loadAccountPage = (event) => {
    this.setState({ render: true });
  };

  loadTodoPage = (event) => {
    this.setState({ render: false });
  };

  logoutHandler = (event) => {
    localStorage.removeItem("AuthToken");
    this.props.history.push("/login");
  };

  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      profilePicture: "",
      uiLoading: true,
      imageLoading: false,
    };
  }

  componentDidMount = () => {
    authMiddleWare(this.props.history);

    const authToken = localStorage.getItem("AuthToken");
    console.log(authToken);
    axios.defaults.headers.common = { Authorization: `${authToken}` };
    axios
      .get("/user")
      .then((response) => {
        console.log(response.data);
        this.setState({
          firstName: response.data.userCredentials.firstName,
          lastName: response.data.userCredentials.lastName,
          email: response.data.userCredentials.email,
          zipCode: response.data.userCredentials.zipCode,
          username: response.data.userCredentials.username,
          uiLoading: false,
          profilePicture: response.data.userCredentials.imageUrl,
        });
      })
      .catch((error) => {
        if (error.response.status === 403) {
          this.props.history.push("/login");
        }
        console.log(error);
        this.setState({ errorMsg: "Error in retrieving the data" });
      });
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Navbar />
        <div className={classes.root}>
          {this.state.uiLoading ? (
            <CircularProgress color="secondary" style={{ marginTop: "20%" }} />
          ) : (
            <Projects user_email={this.state.email} />
          )}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(home);
