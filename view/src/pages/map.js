import React, { Component } from "react";
import axios from "axios";

import withStyles from "@material-ui/core/styles/withStyles";
import { authMiddleWare } from "../util/auth";
import MapView from "../components/map";
import Navbar from "../components/navbar";
import CssBaseline from "@material-ui/core/CssBaseline";

const styles = (theme) => ({
  root: {
    display: "flex",
    flexGrow: 1,
    margin: "0px",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },

  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  avatar: {
    height: 110,
    width: 100,
    flexShrink: 0,
    flexGrow: 0,
    marginTop: 20,
  },
  uiProgess: {
    position: "fixed",
    zIndex: "1000",
    height: "31px",
    width: "31px",
    left: "50%",
    top: "35%",
  },
  toolbar: theme.mixins.toolbar,
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
});

class map extends Component {
  state = {
    render: false,
  };

  logoutHandler = (event) => {
    localStorage.removeItem("AuthToken");
    this.props.history.push("/login");
  };

  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      profilePicture: "",
      uiLoading: true,
      imageLoading: false,
    };
  }

  componentWillMount = () => {
    authMiddleWare(this.props.history);

    const authToken = localStorage.getItem("AuthToken");
    axios.defaults.headers.common = { Authorization: `${authToken}` };
  };

  render() {
    return (
      <div>
        <CssBaseline />

        <Navbar />
        <MapView />
      </div>
    );
  }
}

export default withStyles(styles)(map);
