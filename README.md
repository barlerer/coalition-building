# Rotterdambition
A platform for building coalitions, intended to use by residents, businesses and academic institutes!🤝
![Project intorduction](docs/images/rotterdambition.png)

## Who is behind this?
This project was started after academic research intended to find the best coalition building platform for every stakeholder in East Rotterdam area.
This group project is a part of Leiden-Delft-Erasmus 🏫 minor, "Smart and Shared cities".

## How does it work?
Once you register/sign-in, you will be prompted to view the projects where you can like them or dislike them, depending if you found them to your liking. For now, emails will be presented for the process of coalition building. In the coming future, we are planning to add chat functionality, in order to maintain privacy!
![Regular view of the projects](docs/gifs/regularview.gif)

If you want to view the upcoming projects based on a map view, you can easily do so!
![Map view of the projects](docs/gifs/mapview.gif)
## What is the technology stack?
* Serverless express framework running on firebase
* React ⚛️ for the front-end

## There is an important feature I think is missing. What can I do about this?
We wanted this tool to be for the community, by the community 🌎 and we encourage people to fork this repository and submit merge requests with new and additional features!⚡
If you have no development experience but you still think you have an important addition, please open an issue with your suggestions!
This tool is still in development, and is missing many important features. This is for demonstration purposes for what we want to achieve.


## What is the goal of this project?
Please view the [user story](docs/user_scenario.png) to view the goal of this project

## How to run:
You can run the local React app:
```
1. clone the repo
2. cd "view"
3. run "npm install"
4. run "npm start"
```

There is also a deployed version at:

https://barlerer.gitlab.io/coalition-building

## Contact information:
Bar Lerer, b.lerer@student.tudelft.nl  
Vera Safronova, lanskayavera@gmail.com  
Julie Leclercq, julieleclercq2000@gmail.com  
Louise van den Wildenberg, L.vandenWildenberg@student.tudelft.nl

## Pamphlet:
![Project intorduction](docs/images/Rotterdambition-1.png)
![Project intorduction](docs/images/Rotterdambition_back_1.png)

